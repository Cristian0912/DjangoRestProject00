from rest_framework import generics, status
from rest_framework.response import Response
from django.http import HttpResponse
from rest_framework.renderers import JSONRenderer
from rest_framework.decorators import api_view
from apps.Mascotas.models import Pet
from apps.Mascotas.serializer import PetSerializer

"""El codigo comentado hace lo necesario para retornar en json pero quize intentar entendiendo 
otras maneras de hacerlo, pero cuando estaba registrando, no me funcionaba siempre me pedía un argumento"""
'''class Petlist(generics.ListCreateAPIView):
    queryset = Pet.objects.all()
    serializer_class = PetSerializer

class PetDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Pet.objects.all()
    serializer_class = PetSerializer'''
""""""

"""Creamos nuestra clase JSONResponse, para retornar en formato JSON."""


class JSONResponse(HttpResponse):
  def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

"""Utilizamos el decorador @api_view que captura el tipo de metodo recivido
Y realiza las acciones necesarias, si es get, retorna a las mascotas, si es Post, valida
que los datos requeridos por el modelo mascota sean validos y si es put, actualiza, y si es delete, elimina.
Y dependiendo de cada accion se retorna un estado junto con la data serealizada."""

@api_view(['GET', 'POST'])
def Pet_list(request):

    if request.method == 'GET':
        mascota = Pet.objects.all()
        serializer = PetSerializer(mascota,many=True)
        print(repr(serializer.data))
        return Response(serializer.data)
    elif request.method == 'POST':
        serializer = PetSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'PUT', 'DELETE'])
def Pet_detail(request, pk):
    try:
        mascota = Pet.objects.get(pk=pk)
    except Pet.DoesNotExist:
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

    if request.method == 'GET':
        serializer = PetSerializer(mascota)
        return JSONResponse(serializer.data)

    elif request.method == 'PUT':
        serializer = PetSerializer(mascota, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return JSONResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        mascota.delete()
        return HttpResponse(status=204)
