from django.apps import AppConfig

"""Configuramos nuestra app. Y de damos un name."""
class MascotasConfig(AppConfig):
    name = 'apps.Mascotas'
