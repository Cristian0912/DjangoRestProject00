from rest_framework import serializers
from apps.Mascotas.models import Pet


"""Creamos nuestro serealizador, que nos permite serealizar los datos de nuestro modelo Pet"""
class PetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pet
        fields = ('name', 'kind', 'birth_date')