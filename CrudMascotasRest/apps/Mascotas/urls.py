from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from apps.Mascotas.views import Pet_list, Pet_detail

"""urlpatterns = [
    url(r'^mascotas/$', Petlist.as_view(), name="mascota_l"),
    url(r'^mascotas/(?P<pk>\d+)/$', Petdetail.as_view(), name="mascota_del_up"),
]"""
"""Configuración de las Url´s, aquí requerimos los views creados, y a cada uno le damos un name."""
urlpatterns = [
    url(r'^mascotas/$', Pet_list, name="mascota_l"),
    url(r'^mascotas/(?P<pk>\d+)/$', Pet_detail, name="mascota_del_up"),
]
urlpatterns = format_suffix_patterns(urlpatterns)