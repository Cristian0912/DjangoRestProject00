from django.db import models
from pygments.lexer import default
from pygments.lexers import get_all_lexers
from pygments.styles import get_all_styles
# Create your models here.

"""Modelo del tipo de mascota, sirve para seleccionar un tipo de mascota previamente registrado"""
class Tipo(models.Model):
    tipo_clase = models.CharField(max_length=25, blank=False, default='')

    def __str__(self):
        return '{}'.format(self.tipo_clase)

"""Modelo de Mascota, solo se ingresa el nombre, el tipo y la fecha son seleccionables."""
class Pet(models.Model):

    name = models.CharField(max_length=25, blank=False, default='')
    birth_date = models.DateField(null=False)
    kind = models.ManyToManyField(Tipo, blank=True)

    class Meta():
        db_table = 'Pet_table'
        ordering = ('name',)

